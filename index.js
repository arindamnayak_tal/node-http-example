var express = require("express");
var app = express();
var request = require("request");
//var error_in_fwd = "";
var response ;

app.get('/', function(req, res){
	var url = "http://localhost:3001" + req.url;
	response = res;
	req.pipe(request(url).on("error",errorHandler)).pipe(res);
	
});

function errorHandler(err) {
    response.status(500).send("Error occoured while forwarding request");
}

process.on('uncaughtException', function(err) {
  console.log("Some unhandled error occoured");
  console.log(err);
  console.log("Stopping server");
  process.exit(1);
});

app.listen(3000);